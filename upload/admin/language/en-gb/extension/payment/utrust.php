<?php

// Heading
$_['heading_title'] = 'Utrust';




$_['entry_api_url_help'] = 'https://merchants.api.sandbox-utrust.com/api/ (for sandbox) or  https://merchants.api.utrust.com/api/ (production)';
// Text

$_['text_utrust'] = '<a onclick="window.open(\'https:www.utrust.com\');"><img style="height: auto;max-width: 129px;" src="view/image/payment/utrust.png" alt="utrust.com" title="utrust.com"/><br /></a>';
$_['text_production'] = 'Production';
$_['text_sandbox'] = 'Sandbox';
$_['text_extension'] = 'Extensions';
$_['text_success'] = 'Success: You have modified utrust details!';
$_['text_edit'] = 'Edit utrust';

// Entry
$_['entry_api_token'] = 'API Key';
$_['entry_api_url'] = 'API URL';
$_['entry_webhook_secret'] = 'Webhook Secret';

$_['entry_order_status_pending'] = 'Order Status Pending';
$_['entry_order_status_cancel'] = 'Order Status Cancel';
$_['entry_order_status_failed'] = 'Order Status Failed';
$_['entry_order_status_paid'] = 'Order Status Paid';
$_['entry_geo_zone'] = 'Geo Zone';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sort Order';

$_['error_permission'] = 'Warning: You do not have permission to modify utrust!';
$_['error_mandatory'] = 'Required parameter.';



$_['error_api_url'] = 'You need to fill the request URL!';
$_['error_api_token'] = 'You need to fill the request API key!';
$_['error_webhook_secret'] = 'You need to fill the webhook secret!';
