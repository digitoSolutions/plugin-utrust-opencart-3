<?php

// Heading
$_['heading_title'] = 'Utrust';

// Text
$_['text_production'] = 'Production';
$_['text_sandbox'] = 'Sandbox';
$_['text_extension'] = 'Extensions';
$_['text_success'] = 'Success: You have modified utrust details!';
$_['text_edit'] = 'Edit utrust';
$_['text_utrust'] = '<a onclick="window.open(\'https:www.utrust.com\');"><img style="height: auto;max-width: 129px;" src="view/image/payment/utrust.png" alt="utrust.com" title="utrust.com"/><br /></a>';
$_['entry_api_url_help'] = 'https://merchants.api.sandbox-utrust.com/api/ (para sandbox) ou  https://merchants.api.utrust.com/api/ (para produção)';
// Entry
$_['entry_api_token'] = 'API Token';
$_['entry_api_url'] = 'API URL';
$_['entry_webhook_secret'] = 'Webhook Secret';

$_['entry_order_status_pending'] = 'Order Status Pending';
$_['entry_order_status_cancel'] = 'Order Status Cancel';
$_['entry_order_status_failed'] = 'Order Status Failed';
$_['entry_order_status_paid'] = 'Order Status Paid';
$_['entry_geo_zone'] = 'Geo Zone';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sort Order';

$_['error_permission'] = 'Warning: You do not have permission to modify utrust!';
$_['error_mandatory'] = 'Required parameter.';
$_['error_api_url'] = 'Tem que preencher o URL base dos pedidos!';
$_['error_api_token'] = 'Tem que preencher a chave da API!';
$_['error_webhook_secret'] = 'Tem que preencher o webhook secret!';
