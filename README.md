# Plugin Utrust - Opencart 3

Let your clients pay with crypto on Opencart


## Install instructions

1. Copy the folders inside upload to the root of your opencart installation;
2. Go to the Extensions Tab and click on Modifications;
3. Click on the blue arrows on the top right to refresh the Modifications;
4. Go to Extensions and Choose payment on the dropdown selector;
5. Click on the Utrust entry;
6. Fill the details.

## You're ready to start selling!